echo ===============================
echo Stopping and Removing Container
echo ===============================

docker stop apex19-test
docker container rm apex19-test

echo ===============================
echo Removing Old Images
echo ===============================

docker image rm -f apex19

echo ===============================
echo Building new Image
echo ===============================

docker build -t apex19 --build-arg PASSWORD=test1234 --build-arg APEXPASS=$uperP@ssword1 .

echo ===============================
echo Running new Build Image
echo ===============================

docker run -d --name apex19-test -p 49160:22 -p 8080:8080 -p 1521:1521 apex19

echo ===============================
echo Creating Shell to Container
echo ===============================

docker exec -it apex19-test /bin/sh -c "[ -e /bin/bash ] && /bin/bash || /bin/sh"
