FROM centos:latest

ARG PASSWORD
ENV PASSWORD ${PASSWORD:-secret}
ARG APEXPASS
ENV APEXPASS ${APEXPASS}

ENV ORACLE_HOME /opt/oracle/product/18c/dbhomeXE
ENV PATH $ORACLE_HOME/bin:$PATH
ENV ORACLE_SID=XE
ENV ORACLE_DOCKER_INSTALL=true

EXPOSE 22 1521 8080

COPY scripts /scripts

COPY files /files

RUN bin/bash -c ". /scripts/install_main.sh"

ADD entrypoint.sh /
ENTRYPOINT [ "/entrypoint.sh" ]