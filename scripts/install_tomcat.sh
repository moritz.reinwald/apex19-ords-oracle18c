#!/bin/bash
cd ../files

tar -xzf apache-tomcat-9.0.19.tar.gz
mv apache-tomcat-9.0.19 ../tomcat9
cd ../scripts
sed -i -e 's/password="secret"/password="'$PASSWORD'"/g' tomcat-users.xml
sed -i -e 's/password="secret"/password="'$PASSWORD'"/g' tomcat
mv tomcat-users.xml ../tomcat9/conf/
mv tomcat ../etc/init.d/tomcat/
mv -f context.xml /tomcat9/webapps/manager/META-INF/
chmod 755 ../etc/init.d/tomcat
#update-rc.d tomcat defaults 80 01
