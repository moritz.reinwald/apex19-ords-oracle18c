#!/bin/bash
yum check-update
yum -y  install openssh-server
mkdir ../var/run/sshd
service sshd start
service sshd Stop
chkconfig sshd --add
chkconfig sshd on --level 2,3,4,5
#echo 'root:'$PASSWORD | chpasswd
#sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' ../etc/ssh/sshd_config
#sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i ../etc/pam.d/sshd
#echo "export VISIBLE=now" >> ../etc/profile