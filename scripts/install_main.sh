#!/bin/bash
exec >> >(tee -ai /docker_log.txt)
exec 2>&1
# Update working
yum check-update
yum -y install unzip curl nano #
# Download files
#echo "--------------------------------------------------"
#echo "Downloading all files............................."
#chmod +x download_files.sh
#pwd && ls -l && sh './download_files.sh'
#
#
cd /scripts
# install shh working
echo "--------------------------------------------------"
echo "Installing SSH...................................."
chmod +x install_ssh.sh
sed -i -e 's/\r//g' ./install_ssh.sh
# install_ssh.sh
bash install_ssh.sh
#
echo "--------------------------------------------------"
echo "Installing JAVA..................................."
chmod +x install_java.sh
sed -i -e 's/\r//g' ./install_java.sh
# install_java.sh
bash install_java.sh
#
#
echo "--------------------------------------------------"
echo "Installing TOMCAT................................."
chmod +x install_tomcat.sh
sed -i -e 's/\r//g' ./install_tomcat.sh
# install_tomcat.sh
bash install_tomcat.sh
#
#
echo "--------------------------------------------------"
echo "Installing ORACLE XE.............................."
chmod +x install_oracle.sh
sed -i -e 's/\r//g' ./install_oracle.sh
# install_oracle.sh
bash install_oracle.sh
#
#
echo "--------------------------------------------------"
echo "Installing ORACLE APEX............................"
chmod +x install_apex.sh
#sed -i 's/\r$//g' install_apex.sh
bash install_apex.sh
#
#
echo "--------------------------------------------------"
echo "Installing ORACLE ORDS............................"
chmod +x install_ords.sh
sed -i -e's/\r//g' ./install_ords.sh
# install_ords.sh
bash install_ords.sh
#
#
echo "--------------------------------------------------"
echo "Clean............................................."
echo "Removing temp files"
#/opt/oracle/apex
rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*
rm -rf /opt/ords/ords.war
echo "apt-get clean"
yum clean all
echo "--------------------------------------------------"
echo "Testing.............................................."
java -version
echo "--------------------------------------------------"
echo "DONE.............................................."


