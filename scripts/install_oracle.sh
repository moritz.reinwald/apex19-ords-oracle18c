#!/bin/bash
cd ../scripts
mv chkconfig ../sbin/chkconfig
yum check-update && yum -y install libaio1 nano alien lsb net-tools file yum bc build-essential binutils libcap-dev gcc g++ libc6-dev ksh libaio-dev make libxi-dev libxtst-dev libxau-dev libxcb1-dev sysstat rpm xauth unzip
ln -s ../usr/bin/awk && mkdir ../var/lock/subsys
chmod 755 ../sbin/chkconfig
echo "============================="
echo "Current Dir: " >> pwd 
echo "============================="
echo ls -l
cd ../files
#Installing Dependencys and Oracle XE 
curl -o oracle-database-preinstall-18c-1.0-1.el7.x86_64.rpm https://yum.oracle.com/repo/OracleLinux/OL7/latest/x86_64/getPackage/oracle-database-preinstall-18c-1.0-1.el7.x86_64.rpm
yum -y localinstall oracle-database-preinstall-18c-1.0-1.el7.x86_64.rpm
yum -y install oracle-database-xe-18c-1.0-1.x86_64.rpm
rpm -ivh oracle-instantclient19.3-basic-19.3.0.0.0-1.x86_64.rpm
#Moving init Files
mv /scripts/init.ora /opt/oracle/product/18c/dbhomeXE/dbs/
mv /scripts/initXETemp.ora /opt/oracle/product/18c/dbhomeXE/
printf 8888\\n1521\\n$PASSWORD\\n$PASSWORD\\ny\\n | /etc/init.d/oracle-xe-18c configure
echo 'export ORACLE_HOME=/opt/oracle/product/18c/dbhomeXE' >> /etc/bash.bashrc
echo 'export PATH=$ORACLE_HOME/bin:$PATH' >> /etc/bash.bashrc
echo 'export ORACLE_SID=XE' >> /etc/bash.bashrc
# clean
yum clean all && rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*
rm -rf /files/oracle-xe*