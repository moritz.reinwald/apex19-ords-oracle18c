#!/bin/bash
#PASSWORD=hallo1234
cd /
unzip -o /files/ords-18.4.0.354.1002.zip -d /opt/ords
cd /scripts
sed -i -E 's:apexpw:'$APEXPASS':g' ords_unlock_account.sql
sed -i -E 's:secret:'$PASSWORD':g' ords_unlock_account.sql
sqlplus -S sys/$PASSWORD@XE as sysdba @ords_unlock_account.sql

sed -i -E 's:secret:'$PASSWORD':g' ords_params.properties
cp -rf ords_params.properties /opt/ords/params
cd ../opt/ords
java -jar ords.war configdir /opt/ords/config/
java -jar ords.war install simple --parameterFile /scripts/ords_params.properties

# solution for the problem with timezone
#dpkg-reconfigure tzdata
#echo "Europe/Warsaw" > /etc/timezone
#dpkg-reconfigure -f noninteractive tzdata
cd /
cp -rf /opt/ords/ords.war /tomcat9/webapps/
cp -rf /opt/oracle/apex/images /tomcat9/webapps/i