#!/bin/bash
cd /
exec >> >(tee -ai /docker_log.txt)
exec 2>&1
ls -l
# # Update hostname
sed -i -E "s/HOST = [^)]+/HOST = $HOSTNAME/g" /opt/oracle/product/18c/dbhomeXE/network/admin/listener.ora
sed -i -E "s/HOST = [^)]+/HOST = $HOSTNAME/g" /opt/oracle/product/18c/dbhomeXE/network/admin/tnsnames.ora
sed -i -E "s/PORT = [^)]+/PORT = 1521/g" /opt/oracle/product/18c/dbhomeXE/network/admin/listener.ora
#
./etc/init.d/oracle-xe-18c start
./tomcat9/bin/catalina.sh start
service sshd start
##
## Workaround for graceful shutdown. ....ing oracle... ‿( ́ ̵ _-`)‿
##
while [ "$END" == '' ]; do
	sleep 1
	trap "/etc/init.d/oracle-xe-18c stop && END=1" INT TERM
done
;;